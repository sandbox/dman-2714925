# Install

This module uses a stand-alone php library that runs the scraping process as
 a web service.

    https://github.com/dman-coders/oembed_scraper

The relevant code should be installed via composer.
This can be managed by enabling the composer_manager drupal module,
 as described in
  https://dev.acquia.com/blog/using-composer-manager-get-island-now

Once installed properly, the library files will be found in
  sites/all/vendor/dman-coders/oembed_scraper.

(Variations on the site 'vendor' folder are allowed, manged by composer_manager)

composer manager is expected to have run

    composer update

and fetched additional requirements.

There is a limitation within composer that will not load dependencies of a
custom repository. Thus, including dman-coders/oembed_scraper will not
recursively pull in embed/embed as expected.
The current work-around is to just explicitly name embed/embed at this level.

** Check it's there

The endpoint is now available at the filepath

    sites/all/vendor/dman-coders/oembed_scraper/oembed_scraper.php

You should test this endpoint by visiting the URL directly in your browser.
