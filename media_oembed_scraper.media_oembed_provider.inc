<?php

/**
 * @file
 * Default provider configuration for this service.
 */

/**
 * Implements hook_default_media_oembed_provider().
 */
function media_oembed_scraper_default_media_oembed_provider() {
  $providers = array();

  // Calculate the provider URL based on when in our own site the library
  // was installed.
  $oembed_scraper = libraries_detect('oembed_scraper');
  $callback_path = $oembed_scraper['library path'] . '/' . $oembed_scraper['path'] . '/oembed_scraper.php';
  $callback_url = url($callback_path, array('absolute' => TRUE));

  $provider = new stdClass();
  $provider->disabled = FALSE; /* Edit this to true to make a default provider disabled initially */
  $provider->api_version = 1;
  $provider->name = 'scraper';
  $provider->admin_title = 'oEmbed Scraper';
  $provider->admin_description = 'This service attempts to retrieve metadata directly from the given URLs.';
  $provider->endpoint = $callback_url;
  $provider->scheme = '*
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
';
  $providers['scraper'] = $provider;

  return $providers;
}
